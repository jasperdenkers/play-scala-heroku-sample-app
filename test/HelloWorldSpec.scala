import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._

class ApplicationSpec extends PlaySpec with OneAppPerTest {

  "HelloWorld" should {

    "return 'Hello World!' on index" in {
      val index = route(app, FakeRequest(GET, "/")).get

      status(index) mustBe OK
      contentType(index) mustBe Some("text/plain")
      contentAsString(index) must include ("Hello World!")
    }

  }

}
