# Sample Play Framework Scala application

[![Build Status](https://gitlab.com/jasperdenkers/play-scala-heroku-sample-app/badges/master/build.svg)](https://gitlab.com/jasperdenkers/play-scala-heroku-sample-app/badges/master/build.svg)

An example app to demonstrate the usage of Gitlab CI for testing and deploying Play Framework applications with Scala to Heroku. A running version of this app can be found [here](https://gitlab-play-sample-app.herokuapp.com/).

## Versions

  - [Play](https://www.playframework.com/) 2.5.0
  - [Scala](http://www.scala-lang.org/) 2.11.8
  - [SBT](http://www.scala-sbt.org/) 0.13.11
  - [ScalaTest + Play](http://www.scalatest.org/plus/play) 1.5.0-RC1

## Prerequisites
You will need to have **Scala** and **sbt** installed to run the project.

## Run app locally
Execute `sbt run` in the project root directory. Your app will be available at http://localhost:9000.

## Run tests
Execute `sbt test` in the project root directory.
